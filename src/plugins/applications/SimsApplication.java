package plugins.applications;

import application.interfaces.IDisplay;
import application.interfaces.IPersonFactory;
import application.interfaces.IThingFactory;

import java.util.ArrayList;
import java.util.List;

import application.interfaces.IAction;
import application.interfaces.IApplication;
import application.models.Building;
import application.models.Person;
import application.models.Thing;
import application.platform.Loader;

public class SimsApplication implements IApplication {
    private Loader loader;

    private IDisplay display;
    private IPersonFactory personFactory;
    private IThingFactory thingFactory;
    private List<IAction> actions;

    public SimsApplication(IDisplay display, IPersonFactory personFactory, IThingFactory thingFactory,
            IAction eatAction, IAction sleepAction, IAction readAction) {
        this.display = display;
        this.personFactory = personFactory;
        this.thingFactory = thingFactory;
        this.actions = new ArrayList<IAction>();
        this.actions.add(sleepAction);
        this.actions.add(eatAction);
        this.actions.add(readAction);
    }

    public void run() {
        List<Person> sims = this.personFactory.create();
        List<Thing> things = this.thingFactory.create();

        // Convert things to building
        List<Building> buildings = new ArrayList<Building>();
        for (Thing thing : things) {
            buildings.add((Building) thing);
        }

        this.display.displayAll(sims, buildings, this.actions);
        return;
    }

    public void setLoader(Loader loader) {
        this.loader = loader;
    }
}