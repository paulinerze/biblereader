package plugins.displays;

import java.util.List;

import application.interfaces.IAction;
import application.interfaces.IDisplay;
import application.models.Building;
import application.models.Person;

public class ConsoleDisplay implements IDisplay {
	public void displayPerson(Person p) {
		System.out.println(p);
	}

	public void displayAll(List<Person> persons, List<Building> buildings, List<IAction> actions) {

	}
}
