package plugins.actions;

import java.util.ArrayList;
import java.util.List;

import application.interfaces.IAction;
import application.models.Consumable;
import application.models.Person;
import application.models.Thing;

public class EatAction implements IAction {
    public void doAction(List<Person> persons, List<Thing> things) {
        List<Consumable> consumables = new ArrayList<Consumable>();

        for (Thing thing : things) {
            consumables.add((Consumable) thing);
        }

        for (Person person : persons) {
            for (Consumable consumable : consumables) {
                person.setHunger(person.getHunger() + consumable.getFood());
                person.setThirst(person.getThirst() + consumable.getDrink());
                person.setEnergy(person.getEnergy() + consumable.getEnergy());
                consumable.consume();
            }
        }
    }

    @Override
    public String toString() {
        return "Eat";
    }
}