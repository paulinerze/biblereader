package plugins.actions;

import java.util.List;

import application.interfaces.IAction;
import application.models.Person;
import application.models.Thing;

public class ReadAction implements IAction {
    public void doAction(List<Person> persons, List<Thing> things) {
        for (Person person : persons) {
            if (things.get(0) != null) {
                if (things.get(0).getName().startsWith("Bible", 0) && person.getEnergy() >= 10) {
                    person.setFaith(person.getFaith() + 10);
                    person.setEnergy(person.getEnergy() - 10);
                }
            }
        }
    }

    @Override
    public String toString() {
        return "Read";
    }
}