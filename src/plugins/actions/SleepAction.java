package plugins.actions;

import java.util.List;

import application.interfaces.IAction;
import application.models.Person;
import application.models.Thing;

public class SleepAction implements IAction {
    public void doAction(List<Person> persons, List<Thing> things) {
        for (Person person : persons) {
            person.setEnergy(person.getEnergy() + 80);
        }
    }

    @Override
    public String toString() {
        return "Sleep";
    }
}