package application.interfaces;

import java.util.List;

import application.models.Person;

public interface IPersonFactory {
    public List<Person> create();
}