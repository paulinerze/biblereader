package application.interfaces;

import java.util.List;

import application.models.Building;
import application.models.Person;

public interface IDisplay {
	void displayPerson(Person p);

	void displayAll(List<Person> persons, List<Building> buildings, List<IAction> actions);
}