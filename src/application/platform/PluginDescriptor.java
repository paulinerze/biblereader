package application.platform;

import java.util.ArrayList;
import java.util.Objects;

public class PluginDescriptor {
	String name;
	String className;
	String interfaceName;
	String description;
	String status = "not loaded";
	ArrayList<String> dependencies;
	Object loadedPlugin = null;

	public PluginDescriptor() {
	}

	public PluginDescriptor(String name, String className, String interfaceName, String description, String status,
			ArrayList<String> dependencies) {
		this.name = name;
		this.className = className;
		this.interfaceName = interfaceName;
		this.description = description;
		this.status = status;
		this.dependencies = dependencies;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getClassName() {
		return this.className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getInterfaceName() {
		return this.interfaceName;
	}

	public void setInterfaceName(String interfaceName) {
		this.interfaceName = interfaceName;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public ArrayList<String> getDependencies() {
		return this.dependencies;
	}

	public void setDependencies(ArrayList<String> dependencies) {
		this.dependencies = dependencies;
	}

	public Object getLoadedPlugin() {
		return this.loadedPlugin;
	}

	public void setLoadedPlugin(Object loadedPlugin) {
		this.loadedPlugin = loadedPlugin;
	}

	public PluginDescriptor name(String name) {
		this.name = name;
		return this;
	}

	public PluginDescriptor className(String className) {
		this.className = className;
		return this;
	}

	public PluginDescriptor interfaceName(String interfaceName) {
		this.interfaceName = interfaceName;
		return this;
	}

	public PluginDescriptor description(String description) {
		this.description = description;
		return this;
	}

	public PluginDescriptor status(String status) {
		this.status = status;
		return this;
	}

	public PluginDescriptor dependencies(ArrayList<String> dependencies) {
		this.dependencies = dependencies;
		return this;
	}

	public PluginDescriptor loadedPlugin(Object loadedPlugin) {
		this.loadedPlugin = loadedPlugin;
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this)
			return true;
		if (!(o instanceof PluginDescriptor)) {
			return false;
		}
		PluginDescriptor pluginDescriptor = (PluginDescriptor) o;
		return Objects.equals(name, pluginDescriptor.name) && Objects.equals(className, pluginDescriptor.className)
				&& Objects.equals(interfaceName, pluginDescriptor.interfaceName)
				&& Objects.equals(description, pluginDescriptor.description)
				&& Objects.equals(status, pluginDescriptor.status)
				&& Objects.equals(dependencies, pluginDescriptor.dependencies)
				&& Objects.equals(loadedPlugin, pluginDescriptor.loadedPlugin);
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, className, interfaceName, description, status, dependencies, loadedPlugin);
	}

	@Override
	public String toString() {
		return "{" + " name='" + getName() + "'" + ", className='" + getClassName() + "'" + ", interfaceName='"
				+ getInterfaceName() + "'" + ", description='" + getDescription() + "'" + ", status='" + getStatus()
				+ "'" + ", dependencies='" + getDependencies() + "'" + ", loadedPlugin='" + getLoadedPlugin() + "'"
				+ "}";
	}

}
