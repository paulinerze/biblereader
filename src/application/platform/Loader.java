package application.platform;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import application.interfaces.IApplication;
import libs.json.Json;
import libs.json.JsonArray;
import libs.json.JsonObject;
import libs.json.JsonValue;
import libs.json.JsonObject.Member;

public class Loader {
	public HashMap<String, PluginDescriptor> availablePlugins;
	private String pluginsPath;

	public static void main(String[] args) throws Exception {
		Loader loader = new Loader();

		// First way of loading a plugin (using config.json)
		IApplication application = (IApplication) loader.getConfiguredPlugin("application.interfaces.IApplication");

		// Second way of loading a plugin (manually)
		// List<Object> dependencies = new ArrayList<Object>();
		// dependencies.add(loader.getPlugin(loader.availablePlugins.get("afficheur1")));
		// dependencies.add(loader.getPlugin(loader.availablePlugins.get("person-factory1")));
		// dependencies.add(loader.getPlugin(loader.availablePlugins.get("thing-factory1")));
		// IApplication application = (IApplication)
		// loader.getPlugin(loader.availablePlugins.get("application1"),
		// dependencies);

		application.setLoader(loader);
		application.run();

	}

	private Loader() {
		String pathToConfigs = new File("").getAbsoluteFile().getAbsolutePath().concat(File.separator + "resources");
		this.pluginsPath = pathToConfigs;
		this.initialLoad(pathToConfigs);
	}

	private Loader(String pluginsPath) {
		this.pluginsPath = pluginsPath;
		this.initialLoad(pluginsPath);
	}

	public HashMap<String, PluginDescriptor> getPluginDescriptors(String interfaceName) {
		if (interfaceName.isEmpty()) {
			return this.availablePlugins;
		}

		HashMap<String, PluginDescriptor> result = new HashMap<String, PluginDescriptor>();

		for (Map.Entry<String, PluginDescriptor> entry : this.availablePlugins.entrySet()) {
			if (interfaceName.equals(entry.getValue().getInterfaceName())) {
				result.put(entry.getKey(), entry.getValue());
			}
		}

		return result;
	}

	/**
	 * Instanciate a plugin using a plugin descriptor that have dependencies
	 * 
	 * @param pluginDescriptor the plugin that you want to load
	 * @param dependencies     the plugin's instantiated dependencies (put in the
	 *                         right order, from the available plugins file
	 *                         'available-plugins.json')
	 * @return Object
	 */
	public Object getPlugin(PluginDescriptor pluginDescriptor, List<Object> dependencies) {
		if (pluginDescriptor.getStatus().equals("loader")) {
			return pluginDescriptor.getLoadedPlugin();
		}

		if (dependencies.size() == 0) {
			return this.getPlugin(pluginDescriptor);
		}

		// Get the interface name of the dependencies to instantiate the plugin
		Class<?>[] dependenciesNames = new Class<?>[pluginDescriptor.getDependencies().size()];

		try {
			for (int i = 0; i < pluginDescriptor.getDependencies().size(); i++) {
				dependenciesNames[i] = Class.forName(pluginDescriptor.getDependencies().get(i));
			}
		} catch (ClassNotFoundException | SecurityException e) {
			e.printStackTrace();
			pluginDescriptor.setStatus("failed");
			return null;
		}

		try {
			Object plugin = Class.forName(pluginDescriptor.getClassName()).getDeclaredConstructor(dependenciesNames)
					.newInstance(dependencies.toArray());
			pluginDescriptor.setLoadedPlugin(plugin);
			pluginDescriptor.setStatus("loaded");
			return plugin;
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException | NoSuchMethodException
				| SecurityException | InvocationTargetException e) {
			e.printStackTrace();
			pluginDescriptor.setStatus("failed");
			return null;
		}
	}

	/**
	 * Instanciate a plugin using a plugin descriptor that doesn't have dependencies
	 * 
	 * @param pluginDescriptor the plugin that you want to load
	 * 
	 * @return Object
	 */
	public Object getPlugin(PluginDescriptor pluginDescriptor) {
		if (pluginDescriptor.getStatus().equals("loaded")) {
			return pluginDescriptor.getLoadedPlugin();
		}

		try {
			Object plugin = Class.forName(pluginDescriptor.getClassName()).getDeclaredConstructor().newInstance();
			pluginDescriptor.setLoadedPlugin(plugin);
			pluginDescriptor.setStatus("loaded");
			return plugin;
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException | NoSuchMethodException
				| SecurityException | InvocationTargetException e) {
			e.printStackTrace();
			pluginDescriptor.setStatus("failed");
			return null;
		}
	}

	private void initialLoad(String pluginsPath) {
		this.availablePlugins = new HashMap<String, PluginDescriptor>();
		try {
			this.loadAvailablePlugins(pluginsPath);
		} catch (Exception e) {
			System.out.println("Available plugins file (" + pluginsPath + ") failed to load :");
			e.printStackTrace();
		}
	}

	private void loadAvailablePlugins(String pluginsPath) throws Exception {
		File availablePluginsJsonFile = new File(pluginsPath + File.separator + "available-plugins.json");

		if (!availablePluginsJsonFile.exists()) {
			throw new FileNotFoundException();
		}

		JsonObject availablePluginsJson = Json.parse(new FileReader(availablePluginsJsonFile)).asObject();

		for (Member interfaceJson : availablePluginsJson) {
			String interfaceName = interfaceJson.getName();
			JsonObject plugins = interfaceJson.getValue().asObject();

			for (Member plugin : plugins) {
				String pluginName = plugin.getName();
				JsonObject pluginDetail = plugin.getValue().asObject();

				this.availablePlugins.put(pluginName,
						this.jsonToPluginDescriptor(pluginDetail, interfaceName, pluginName));
			}
		}
	}

	/**
	 * Instanciate a plugin using it's interface name (uses the config.json file to
	 * detect the right plugin and its the dependencies)
	 * 
	 * @param interfaceName name of the interface, like
	 *                      "application.interfaces.IApplication"
	 * 
	 * @return Object
	 */
	public Object getConfiguredPlugin(String interfaceName) throws Exception {
		File configJsonFile = new File(pluginsPath + File.separator + "config.json");

		if (!configJsonFile.exists()) {
			throw new FileNotFoundException();
		}

		JsonObject configJson = Json.parse(new FileReader(configJsonFile)).asObject();

		if (!configJson.contains(interfaceName)) {
			throw new Exception(interfaceName + " : Interface does not exist");
		}

		JsonObject interfaceJson = configJson.get(interfaceName).asObject();

		if (!this.validatePluginConfiguration(interfaceJson)) {
			throw new Exception(interfaceName + " : Not a valid plugin object");
		}

		return this.getPluginFromInterfaceJson(interfaceName, interfaceJson);
	}

	private Object getPluginFromInterfaceJson(String interfaceName, JsonObject interfaceJson) throws Exception {
		String pluginName = interfaceJson.getString("name", "");

		PluginDescriptor pluginDescriptor = this.availablePlugins.get(pluginName);

		if (pluginDescriptor == null) {
			throw new Exception(interfaceName + " : '" + pluginName + "' does not exist (is it defined in 'resources"
					+ File.separator + "available-plugins.json' ?)");
		}

		// without dependencies
		if (pluginDescriptor.getDependencies().size() == 0) {
			return this.getPlugin(pluginDescriptor);
		}

		JsonArray dependenciesJson = interfaceJson.get("dependencies").asArray();
		ArrayList<Object> dependencies = new ArrayList<Object>();

		for (JsonValue dependencyJson : dependenciesJson) {
			JsonObject dependencyJsonObject = dependencyJson.asObject();

			if (!this.validatePluginConfiguration(dependencyJsonObject)) {
				throw new Exception(interfaceName + " : Not a valid plugin dependency");
			}

			dependencies.add(this.getPluginFromInterfaceJson(interfaceName, dependencyJsonObject));
		}

		return this.getPlugin(pluginDescriptor, dependencies);
	}

	private boolean validatePluginConfiguration(JsonObject pluginConfiguration) {
		return pluginConfiguration.contains("name") && pluginConfiguration.get("name").isString()
				&& (pluginConfiguration.contains("dependencies") && pluginConfiguration.get("dependencies").isArray());
	}

	private PluginDescriptor jsonToPluginDescriptor(JsonObject json, String interfaceName, String pluginName)
			throws Exception {
		if (!json.contains("class")) {
			throw new Exception(
					interfaceName + " (" + pluginName + ") : Not a valid plugin object (missing 'class' attribute)");
		}

		if (!json.contains("description")) {
			throw new Exception(interfaceName + " (" + pluginName
					+ ") : Not a valid plugin object (missing 'description' attribute)");
		}

		if (!json.contains("dependencies")) {
			throw new Exception(interfaceName + " (" + pluginName
					+ ") : Not a valid plugin object (missing 'dependencies' attribute)");
		}

		JsonArray dependenciesJson = json.get("dependencies").asArray();
		ArrayList<String> dependencies = new ArrayList<String>();

		for (JsonValue jsonValue : dependenciesJson) {
			JsonObject dependence = jsonValue.asObject();

			if (!dependence.contains("interface")) {
				throw new Exception(interfaceName + " (" + pluginName
						+ ") : Not a valid plugin dependency (missing 'interface' attribute in dependence)");
			}

			dependencies.add(dependence.getString("interface", ""));
		}

		return new PluginDescriptor(pluginName, json.getString("class", ""), interfaceName,
				json.getString("description", ""), "not loaded", dependencies);
	}
}